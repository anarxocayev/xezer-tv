package az.xezertv.minioms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinIOMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinIOMsApplication.class, args);
    }
}
