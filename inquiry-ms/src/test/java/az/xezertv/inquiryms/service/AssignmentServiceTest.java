package az.xezertv.inquiryms.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.inquiryms.dto.AssignmentDto;
import az.xezertv.inquiryms.entity.Assignment;
import az.xezertv.inquiryms.repository.AssignmentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AssignmentServiceTest {
    @Mock
    AssignmentRepository assignmentRepository;

    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    AssignmentService assignmentService;

    List<Assignment> findAll() {
        List<Assignment> assignments = new ArrayList<>();
        Assignment assignment1 = new Assignment();
        Assignment assignment2 = new Assignment();

        assignments.add(assignment1);
        assignments.add(assignment2);

        return assignments;
    }

    @Test
    void testMockObjectsAreNotNull() {
        assertNotNull(assignmentRepository);
        assertNotNull(assignmentService);
        assertNotNull(modelMapper);
    }

    @Test
    void testFindAllSuccess() {
        when(assignmentRepository.findAll()).thenReturn(findAll());

        List<AssignmentDto> assignmentDtos = assignmentService.findAll();
        assertEquals(assignmentDtos.size(), 2);
    }

    @Test
    void testFindByIdSuccess() {
        Assignment assignment = new Assignment();
        assignment.setId(1L);
        assignment.setNote("Note");
        assignment.setStartDate(LocalDate.of(2022, 9, 15));

        AssignmentDto assignmentDto = new AssignmentDto();
        assignmentDto.setId(1L);
        assignmentDto.setNote("Note");
        assignmentDto.setStartDate(LocalDate.of(2022, 9, 15));

        when(assignmentRepository.findById(assignment.getId())).thenReturn(Optional.of(assignment));
        when(assignmentService.findById(assignmentDto.getId())).thenReturn(assignmentDto);

        AssignmentDto assignmentDto1 = assignmentService.findById(assignment.getId());
        assertEquals(assignment.getId(), assignmentDto1.getId());
    }

    @Test
    void testFindByIdNotFoundException() {
        Assignment assignment = new Assignment();
        assignment.setId(1L);
        assignment.setNote("Note");

        given(assignmentRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        assertThrows(NotFoundException.class, () -> assignmentService.findById(assignment.getId()));
    }

    @Test
    void testSaveSuccess() {
        Assignment assignment = new Assignment();
        assignment.setId(1L);
        assignment.setNote("Note");

        AssignmentDto assignmentDto = new AssignmentDto();
        assignmentDto.setId(1L);
        assignmentDto.setNote("Note");

        when(modelMapper.map(assignmentDto, Assignment.class)).thenReturn(assignment);
        when(assignmentRepository.save(assignment)).thenReturn(assignment);
        when(modelMapper.map(assignment, AssignmentDto.class)).thenReturn(assignmentDto);

        AssignmentDto assignmentDto1 = assignmentService.save(assignmentDto);
        assertEquals(assignmentDto1.getNote(), assignmentDto.getNote());
    }

    @Test
    void testUpdateSuccess() {
        Assignment assignment = new Assignment();
        assignment.setId(1L);
        assignment.setNote("Note");
        assignment.setStartDate(LocalDate.of(2022, 9, 15));

        AssignmentDto assignmentDto = new AssignmentDto();
        assignmentDto.setId(1L);
        assignmentDto.setNote("Note");
        assignment.setStartDate(LocalDate.of(2022, 9, 15));

        when(assignmentRepository.findById(assignment.getId())).thenReturn(Optional.of(assignment));
        when(assignmentService.findById(assignmentDto.getId())).thenReturn(assignmentDto);

        AssignmentDto assignmentDto1 = assignmentService.findById(1L);
        assignmentDto1.setNote(assignmentDto.getNote());

        assertEquals(assignmentDto1.getNote(), assignmentDto.getNote());
    }

    @Test
    void testDeleteSuccess() {
        Assignment assignment = new Assignment();
        assignment.setId(1L);
        assignment.setNote("Note");

        when(assignmentRepository.findById(assignment.getId())).thenReturn(Optional.of(assignment));
        assignmentService.delete(assignment.getId());
        verify(assignmentRepository).delete(assignment);
    }

    @Test
    void testDeleteNotFound() {
        Assignment assignment = new Assignment();
        assignment.setId(1L);
        assignment.setNote("Note");

        given(assignmentRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        assertThrows(NotFoundException.class, () -> assignmentService.delete(assignment.getId()));
    }
}
