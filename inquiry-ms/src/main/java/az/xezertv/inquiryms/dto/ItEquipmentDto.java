package az.xezertv.inquiryms.dto;

import az.xezertv.inquiryms.enums.ItEquipmentResult;
import lombok.Data;

@Data
public class ItEquipmentDto {

    private Long id;
    private String descriptionOfProblems;
    private ItEquipmentResult equipmentResult;

}
