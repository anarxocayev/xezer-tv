package az.xezertv.inquiryms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VacationDto {
    private Long id;
    private LocalDate startDate;
    private LocalDate endDate;
    private String result;
}
