package az.xezertv.inquiryms.dto;

import az.xezertv.inquiryms.enums.PurchaseResult;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PurchaseDto {

    private Long id;
    private String descriptionOfProblems;
    private PurchaseResult punchaseResult;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
