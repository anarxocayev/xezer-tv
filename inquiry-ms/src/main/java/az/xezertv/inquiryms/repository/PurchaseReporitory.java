package az.xezertv.inquiryms.repository;

import az.xezertv.inquiryms.entity.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseReporitory  extends JpaRepository<Purchase,Long> {

}
