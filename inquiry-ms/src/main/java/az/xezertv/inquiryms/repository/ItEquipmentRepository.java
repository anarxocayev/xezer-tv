package az.xezertv.inquiryms.repository;

import az.xezertv.inquiryms.entity.ItEquipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItEquipmentRepository extends JpaRepository<ItEquipment,Long> {
}
