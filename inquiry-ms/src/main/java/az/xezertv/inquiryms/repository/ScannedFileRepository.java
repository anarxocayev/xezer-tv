package az.xezertv.inquiryms.repository;

import az.xezertv.inquiryms.entity.ScannedFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScannedFileRepository extends JpaRepository<ScannedFile, Long> {
}
