package az.xezertv.inquiryms.repository;

import az.xezertv.inquiryms.entity.Vacation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VacationRepository extends JpaRepository<Vacation, Long> {
}
