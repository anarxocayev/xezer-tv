package az.xezertv.inquiryms.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.inquiryms.entity.Assignment;
import az.xezertv.inquiryms.entity.ScannedFile;
import az.xezertv.inquiryms.entity.Vacation;
import az.xezertv.inquiryms.repository.AssignmentRepository;
import az.xezertv.inquiryms.repository.ScannedFileRepository;
import az.xezertv.inquiryms.repository.VacationRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
@RequiredArgsConstructor
public class ScannedFileService {
    private final VacationRepository vacationRepository;
    private final AssignmentRepository assignmentRepository;
    private final ScannedFileRepository scannedFileRepository;
    private Path foundFile;

    public String uploadFile(String fileName, MultipartFile multipartFile, Long id) throws IOException {
        Path uploadDirectory = Paths.get("inquiry-vacation-upload");

        String fileCode = RandomStringUtils.randomAlphanumeric(8);

        try (InputStream inputStream = multipartFile.getInputStream()) {

            Path filePath = uploadDirectory.resolve(fileCode + "-" + fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException ioe) {
            throw new IOException("Error saving uploaded file: " + fileName, ioe);
        }

        Vacation vacation = vacationRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Vacation with id %s not found", id)
        ));

        fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        vacation.setScannedFile(fileName);
        vacationRepository.save(vacation);
        Assignment assignment = assignmentRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Assignment with id %s not found", id)
        ));

        fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        assignment.setScannedFile(fileName);
        assignmentRepository.save(assignment);

        ScannedFile scannedFile = new ScannedFile();
        scannedFile.setFileName(fileName);
        scannedFile.setDownloadUri("/download/" + fileCode);
        scannedFile.setSource("Vacation");
        scannedFile.setVacation(vacation);

        scannedFile.setSource("Assignment");
        scannedFile.setAssignment(assignment);
        scannedFileRepository.save(scannedFile);

        return fileCode;
    }

    public Resource downloadFile(String fileCode) throws IOException {

        Path uploadDirectory = Paths.get("Inquiry-Assignment-Upload");

        Files.list(uploadDirectory).forEach(file -> {
            if (file.getFileName().toString().startsWith(fileCode)) {
                foundFile = file;
                return;
            }
        });

        if (foundFile != null) {
            return new UrlResource(foundFile.toUri());
        }

        return null;
    }
}
