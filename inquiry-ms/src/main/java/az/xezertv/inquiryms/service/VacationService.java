package az.xezertv.inquiryms.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.inquiryms.dto.VacationDto;
import az.xezertv.inquiryms.entity.Vacation;
import az.xezertv.inquiryms.repository.VacationRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VacationService {
    private final VacationRepository vacationRepository;
    private final ModelMapper modelMapper;

    public List<VacationDto> findAll() {
        return vacationRepository.findAll()
                .stream()
                .map(vacation -> modelMapper.map(vacation, VacationDto.class))
                .collect(Collectors.toList());
    }

    public VacationDto findById(Long id) {
        Vacation vacation = vacationRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Vacation with id %d not found", id)
        ));

        return modelMapper.map(vacation, VacationDto.class);
    }

    public VacationDto save(VacationDto vacationDto) {
        Vacation vacation = modelMapper.map(vacationDto, Vacation.class);
        vacationRepository.save(vacation);

        return modelMapper.map(vacation, VacationDto.class);
    }

    public VacationDto update(Long id, VacationDto vacationDto) {
        Vacation vacation = vacationRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Vacation with id %d not found", id)
        ));

        modelMapper.map(vacationDto, vacation, "map");
        vacation.setId(id);

        return modelMapper.map(vacationRepository.save(vacation), VacationDto.class);
    }

    public void delete(Long id) {
        Vacation vacation = vacationRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Vacation with id %d not found", id)
        ));

        vacationRepository.delete(vacation);
    }
}
