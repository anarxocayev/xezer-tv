package az.xezertv.inquiryms.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.inquiryms.dto.AssignmentDto;
import az.xezertv.inquiryms.entity.Assignment;
import az.xezertv.inquiryms.repository.AssignmentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AssignmentService {
    private final AssignmentRepository assignmentRepository;
    private final ModelMapper modelMapper;

    public List<AssignmentDto> findAll() {
        return assignmentRepository.findAll()
                .stream()
                .map(assignment -> modelMapper.map(assignment, AssignmentDto.class))
                .collect(Collectors.toList());
    }

    public AssignmentDto findById(Long id) {
        Assignment assignment = assignmentRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Assignment with id %d not found", id)
        ));

        return modelMapper.map(assignment, AssignmentDto.class);
    }

    public AssignmentDto save(AssignmentDto assignmentDto) {
        Assignment assignment = modelMapper.map(assignmentDto, Assignment.class);
        assignmentRepository.save(assignment);

        return modelMapper.map(assignment, AssignmentDto.class);
    }

    public AssignmentDto update(Long id, AssignmentDto assignmentDto) {
        Assignment assignment = assignmentRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Assignment with id %d not found", id)
        ));

        modelMapper.map(assignmentDto, assignment, "map");
        assignment.setId(id);

        return modelMapper.map(assignmentRepository.save(assignment), AssignmentDto.class);
    }

    public void delete(Long id) {
        Assignment assignment = assignmentRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Assignment with id %d not found", id)
        ));

        assignmentRepository.delete(assignment);
    }
}
