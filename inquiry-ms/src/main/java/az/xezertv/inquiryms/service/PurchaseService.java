package az.xezertv.inquiryms.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.inquiryms.dto.ItEquipmentDto;
import az.xezertv.inquiryms.dto.PurchaseDto;
import az.xezertv.inquiryms.entity.ItEquipment;
import az.xezertv.inquiryms.entity.Purchase;
import az.xezertv.inquiryms.repository.PurchaseReporitory;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PurchaseService {
    private final PurchaseReporitory purchaseReporitory;
    private final ModelMapper modelMapper;

    public List<PurchaseDto> findAll(){
        return purchaseReporitory.findAll().stream().map(
                        purchase->modelMapper.map(purchase,PurchaseDto.class))
                .collect(Collectors.toList());
    }

    public PurchaseDto findByid(Long id){
        Purchase purchase=purchaseReporitory.findById(id).orElseThrow(()-> new NotFoundException(
                String.format("Purchase not found with id - %s", id)
        ));
        return modelMapper.map(purchaseReporitory.save(purchase),PurchaseDto.class);
    }

    public PurchaseDto save(PurchaseDto purchaseDto){
        Purchase purchase=modelMapper.map(purchaseDto,Purchase.class);

        return modelMapper.map(purchaseReporitory.save(purchase),PurchaseDto.class);
    }

    public void delete(Long id){
        Purchase purchase=purchaseReporitory.findById(id).orElseThrow(()-> new NotFoundException(
                String.format("purchase not found with id - %s", id)
        ));
        purchaseReporitory.delete(purchase);
    }
    public PurchaseDto update(Long id,PurchaseDto purchaseDto){
        Purchase purchase=purchaseReporitory.findById(id).orElseThrow(()->new NotFoundException(
                String.format("purchase not found with id - %s", id)
        ));
        modelMapper.map(purchaseDto,purchase,"map");

        purchase.setId(id);
        return modelMapper.map(purchaseReporitory.save(purchase),PurchaseDto.class);
    }
}
