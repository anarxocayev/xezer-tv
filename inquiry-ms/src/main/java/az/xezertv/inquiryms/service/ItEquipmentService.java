package az.xezertv.inquiryms.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.inquiryms.dto.ItEquipmentDto;
import az.xezertv.inquiryms.entity.ItEquipment;
import az.xezertv.inquiryms.repository.ItEquipmentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ItEquipmentService {
    private final ItEquipmentRepository equipmentRepository;

    private final ModelMapper modelMapper;

    public List<ItEquipmentDto> findAll(){
        return equipmentRepository.findAll().stream().map(
                equipment->modelMapper.map(equipment,ItEquipmentDto.class))
                .collect(Collectors.toList());
    }

    public ItEquipmentDto findByid(Long id){
        ItEquipment itEquipment=equipmentRepository.findById(id).orElseThrow(()-> new NotFoundException(
                String.format("Equipment not found with id - %s", id)
        ));
        return modelMapper.map(equipmentRepository.save(itEquipment),ItEquipmentDto.class);

    }

    public ItEquipmentDto save(ItEquipmentDto equipmentDto){
        ItEquipment itEquipment=modelMapper.map(equipmentDto,ItEquipment.class);

        return modelMapper.map(equipmentRepository.save(itEquipment),ItEquipmentDto.class);
    }

    public void delete(Long id){
        ItEquipment equipment=equipmentRepository.findById(id).orElseThrow(()-> new NotFoundException(
                String.format("Equipment not found with id - %s", id)
        ));
        equipmentRepository.delete(equipment);
    }
    public ItEquipmentDto update(Long id,ItEquipmentDto equipmentDto){
        ItEquipment itEquipment=equipmentRepository.findById(id).orElseThrow(()->new NotFoundException(
                String.format("Equipment not found with id - %s", id)
        ));
        modelMapper.map(equipmentDto,itEquipment,"map");

        itEquipment.setId(id);
        return modelMapper.map(equipmentRepository.save(itEquipment),ItEquipmentDto.class);
    }
}
