package az.xezertv.inquiryms.controller;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.inquiryms.entity.ScannedFile;
import az.xezertv.inquiryms.repository.ScannedFileRepository;
import az.xezertv.inquiryms.service.ScannedFileService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/assignments")
@RequiredArgsConstructor
public class ScannedFileController {
    private final ScannedFileRepository scannedFileRepository;
    private final ScannedFileService scannedFileService;

    @PostMapping("/upload/{id}")
    public ResponseEntity<ScannedFile> uploadFile(@RequestParam("file") MultipartFile multipartFile,
                                                  @PathVariable("id") Long id) throws IOException {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());

        String fileCode = scannedFileService.uploadFile(fileName, multipartFile, id);

        ScannedFile scannedFile = scannedFileRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("ScannedFile with id %s not found", id)
        ));
        scannedFile.setDownloadUri("/download/" + fileCode);
        scannedFileRepository.save(scannedFile);

        return new ResponseEntity<>(scannedFile, HttpStatus.OK);
    }

    @GetMapping("/download/{fileCode}")
    public ResponseEntity<?> downloadFile(@PathVariable("fileCode") String fileCode) {
        Resource resource = null;

        try {
            resource = scannedFileService.downloadFile(fileCode);
        } catch (IOException e) {
            return ResponseEntity.internalServerError().build();
        }

        if (resource == null) {
            return new ResponseEntity<>("File not found", HttpStatus.NOT_FOUND);
        }

        String contentType = "application/octet-stream";
        String headerValue = "attachment; filename=\"" + resource.getFilename() + "\"";

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, headerValue)
                .body(resource);
    }
}
