package az.xezertv.inquiryms.controller;

import az.xezertv.inquiryms.dto.VacationDto;
import az.xezertv.inquiryms.service.VacationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/vacations")
@RequiredArgsConstructor
public class VacationController {
    private final VacationService vacationService;

    @GetMapping
    public ResponseEntity<List<VacationDto>> findAll() {
        return ResponseEntity.ok(vacationService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<VacationDto> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(vacationService.findById(id));
    }

    @PostMapping
    public ResponseEntity<VacationDto> save(@RequestBody VacationDto vacationDto) {
        return ResponseEntity.ok(vacationService.save(vacationDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<VacationDto> update(@PathVariable("id") Long id, @RequestBody VacationDto vacationDto) {
        return ResponseEntity.ok(vacationService.update(id, vacationDto));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        vacationService.delete(id);
    }
}
