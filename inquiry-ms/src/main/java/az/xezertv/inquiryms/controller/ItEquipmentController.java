package az.xezertv.inquiryms.controller;

import az.xezertv.inquiryms.dto.ItEquipmentDto;
import az.xezertv.inquiryms.service.ItEquipmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/itequipment")
@RequiredArgsConstructor
public class ItEquipmentController {
    private final ItEquipmentService equipmentService;

    @GetMapping
    public ResponseEntity<List<ItEquipmentDto>> findAll(){
        return ResponseEntity.ok(equipmentService.findAll());
    }

    @GetMapping("/{id}")
    public  ResponseEntity<ItEquipmentDto> findByid(@PathVariable ("id") Long id){
        return ResponseEntity.ok(equipmentService.findByid(id));
    }

    @PostMapping
    public  ResponseEntity<ItEquipmentDto> save(@RequestBody ItEquipmentDto equipmentDto){

        return ResponseEntity.ok(equipmentService.save(equipmentDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ItEquipmentDto> update(@PathVariable("id") Long id,@RequestBody ItEquipmentDto equipmentDto){
        return ResponseEntity.ok(equipmentService.update(id,equipmentDto));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id){
        equipmentService.delete(id);
    }
}
