package az.xezertv.inquiryms.controller;

import az.xezertv.inquiryms.dto.AssignmentDto;
import az.xezertv.inquiryms.service.AssignmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/assignments")
@RequiredArgsConstructor
public class AssignmentController {
    private final AssignmentService assignmentService;

    @GetMapping
    public ResponseEntity<List<AssignmentDto>> findAll() {
        return ResponseEntity.ok(assignmentService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<AssignmentDto> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(assignmentService.findById(id));
    }

    @PostMapping
    public ResponseEntity<AssignmentDto> save(@RequestBody AssignmentDto assignmentDto) {
        return ResponseEntity.ok(assignmentService.save(assignmentDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<AssignmentDto> update(@PathVariable("id") Long id, @RequestBody AssignmentDto assignmentDto) {
        return ResponseEntity.ok(assignmentService.update(id, assignmentDto));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        assignmentService.delete(id);
    }
}
