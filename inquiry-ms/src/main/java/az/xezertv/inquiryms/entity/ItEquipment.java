package az.xezertv.inquiryms.entity;

import az.xezertv.inquiryms.enums.ItEquipmentResult;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;



@Entity
@Data
@Table(name = "itequipment")
public class ItEquipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description_of_problems")
    private String descriptionOfProblems;

    @Enumerated(EnumType.STRING)
    private ItEquipmentResult equipmentResult;

    @CreationTimestamp
    private  LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

}
