package az.xezertv.inquiryms.entity;

import az.xezertv.inquiryms.enums.PurchaseResult;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "purchase")
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description_of_problems")
    private String descriptionOfProblems;

    @Enumerated(EnumType.STRING)
    @Column(name = "purchase_result")
    private PurchaseResult purchaseResult;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;
}
