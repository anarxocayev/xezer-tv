package az.xezertv.employeems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeMcApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeMcApplication.class, args);
    }
}
