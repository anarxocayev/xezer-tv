package az.xezertv.employeems.repository;

import az.xezertv.employeems.entity.Contract;
import az.xezertv.employeems.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContractRepository extends JpaRepository<Contract,Long> {
    List<Contract> findAllByEmployee(Employee employee);
}
