package az.xezertv.employeems.repository;

import az.xezertv.employeems.entity.Department;
import az.xezertv.employeems.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    List<Employee> findAllByDepartment(Department department);

}
