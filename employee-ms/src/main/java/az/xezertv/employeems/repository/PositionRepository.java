package az.xezertv.employeems.repository;

import az.xezertv.employeems.entity.Position;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepository extends JpaRepository<Position, Long> {
}
