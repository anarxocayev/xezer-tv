package az.xezertv.employeems.repository;

import az.xezertv.employeems.entity.Employee;
import az.xezertv.employeems.entity.Experience;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExperienceRepository extends JpaRepository<Experience, Long> {
    List<Experience> findAllByEmployee(Employee employee);

}
