package az.xezertv.employeems.repository;

import az.xezertv.employeems.entity.Employee;
import az.xezertv.employeems.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByEmployee(Employee employee);
}
