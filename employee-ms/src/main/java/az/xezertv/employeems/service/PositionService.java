package az.xezertv.employeems.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.model.dto.PositionDto;
import az.xezertv.employeems.entity.Position;
import az.xezertv.employeems.repository.PositionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PositionService {

    private final PositionRepository positionRepository;
    private final ModelMapper modelMapper;

    public List<PositionDto> getAll() {
        return positionRepository.findAll()
                .stream()
                .map(position -> modelMapper.map(position,PositionDto.class))
                .collect(Collectors.toList());
    }

    public PositionDto getById(Long id) {
        Position position = positionRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Position not found with id - %s", id)
        ));
        return modelMapper.map(positionRepository.save(position), PositionDto.class);
    }

    public PositionDto createPosition(PositionDto positionDto) {
        Position position = modelMapper.map(positionDto, Position.class);
        return modelMapper.map(positionRepository.save(position), PositionDto.class);
    }

    public PositionDto updatePosition(Long id, PositionDto positionDto) {
        Position position = positionRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Position not found with id - %s", id)
        ));

        modelMapper.map(positionDto, position, "map");
        position.setId(id);

        return modelMapper.map(positionRepository.save(position), PositionDto.class);
    }

    public void deletePosition(Long id) {
        Position position = positionRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Position not found with id - %s", id))
        );
        positionRepository.delete(position);
    }
}
