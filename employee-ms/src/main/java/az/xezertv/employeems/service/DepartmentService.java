package az.xezertv.employeems.service;

import az.xezertv.employeems.model.dto.DepartmentDto;
import az.xezertv.employeems.entity.Department;
import az.xezertv.employeems.model.view.DepartmentView;
import az.xezertv.employeems.model.view.EmployeeView;
import az.xezertv.employeems.repository.DepartmentRepository;
import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DepartmentService {
    private final DepartmentRepository departmentRepository;
    private final ModelMapper modelMapper;
    private final EmployeeRepository employeeRepository;

    public List<DepartmentView> findAll() {
        return departmentRepository
                .findAll()
                .stream()
                .map(department -> modelMapper.map(department, DepartmentView.class))
                .collect(Collectors.toList());
    }

    public DepartmentView findById(Long id) {
        Department department = departmentRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(
                String.format("Department not found with id - %s", id)
        ));

        DepartmentView departmentView = modelMapper.map(department, DepartmentView.class);
        departmentView.setEmployees(employeeRepository.findAllByDepartment(department)
                .stream()
                .map(employee -> modelMapper.map(employee, EmployeeView.class))
                .collect(Collectors.toList()));

        return departmentView;
    }

    public DepartmentDto save(DepartmentDto departmentDto) {
        Department department = modelMapper.map(departmentDto, Department.class);
        return modelMapper.map(departmentRepository.save(department), DepartmentDto.class);
    }

    public DepartmentDto update(Long id, DepartmentDto departmentDto) {
        Department department = departmentRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(
                String.format("Department not found with id - %s", id)
        ));

        modelMapper.map(departmentDto, department, "map");
        department.setId(id);

        return modelMapper.map(departmentRepository.save(department), DepartmentDto.class);
    }

    public void delete(Long id) {
        Department department = departmentRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(
                String.format("Department not found with id - %s", id)
        ));
        departmentRepository.delete(department);
    }
}
