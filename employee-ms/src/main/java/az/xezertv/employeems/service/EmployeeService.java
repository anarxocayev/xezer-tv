package az.xezertv.employeems.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.entity.Department;
import az.xezertv.employeems.model.dto.EmployeeDto;
import az.xezertv.employeems.model.dto.OrderDto;
import az.xezertv.employeems.entity.Employee;
import az.xezertv.employeems.model.view.ContractView;
import az.xezertv.employeems.repository.ContractRepository;
import az.xezertv.employeems.model.view.ExperienceView;
import az.xezertv.employeems.entity.Position;
import az.xezertv.employeems.repository.DepartmentRepository;
import az.xezertv.employeems.model.view.EmployeeView;
import az.xezertv.employeems.model.view.OrderView;
import az.xezertv.employeems.repository.EmployeeRepository;
import az.xezertv.employeems.repository.ExperienceRepository;
import az.xezertv.employeems.repository.PositionRepository;
import az.xezertv.employeems.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;
    private final PositionRepository positionRepository;
    private final OrderRepository orderRepository;
    private final ContractRepository contractRepository;
    private final ModelMapper modelMapper;
    private final ExperienceRepository experienceRepository;

    public List<EmployeeView> getAll() {
        return employeeRepository.findAll()
                .stream()
                .map(employee -> modelMapper.map(employee, EmployeeView.class))
                .collect(Collectors.toList());
    }

    public EmployeeView getById(Long id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Employee not found with id - %s", id)
        ));

        EmployeeView employeeView = modelMapper.map(employee, EmployeeView.class);

        employeeView.setOrders(orderRepository.findAllByEmployee(employee)
                .stream()
                .map(order -> modelMapper.map(order, OrderView.class))
                .collect(Collectors.toList()));

        employeeView.setExperiences(experienceRepository.findAllByEmployee(employee)
                .stream()
                .map(experience -> modelMapper.map(experience, ExperienceView.class))
                .collect(Collectors.toList()));

        employeeView.setContracts(contractRepository.findAllByEmployee(employee)
                .stream()
                .map(contract -> modelMapper.map(contract, ContractView.class)).collect(Collectors.toList()));
        return employeeView;
    }

    public EmployeeDto createEmployee(EmployeeDto employeeDto) {
        Department department = departmentRepository.findById(employeeDto.getDepartmentId()).orElseThrow(() ->
                new NotFoundException(String.format("Employee not found with id - %s", employeeDto.getDepartmentId())));


        Position position= positionRepository.findById(employeeDto.getPositionId()).orElseThrow(()->new NotFoundException(
                String.format("Position not found with id - %s", employeeDto.getPositionId())
        ));

        Employee employee = modelMapper.map(employeeDto, Employee.class);
        employee.setDepartment(department);
        employee.setPosition(position);
        return modelMapper.map(employeeRepository.save(employee), EmployeeDto.class);
    }

    public EmployeeDto updateEmployee(Long id, EmployeeDto employeeDto) {
        Employee employee = employeeRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Employee not found with id - %s", id)
        ));
        modelMapper.map(employeeDto, employee, "map");
        employee.setId(id);
        return modelMapper.map(employeeRepository.save(employee), EmployeeDto.class);
    }

    public void deleteEmployee(Long id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Employee not found with id - %s", id))
        );
        employeeRepository.delete(employee);
    }
}
