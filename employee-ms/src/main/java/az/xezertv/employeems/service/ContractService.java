package az.xezertv.employeems.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.model.dto.ContractDto;
import az.xezertv.employeems.entity.Contract;
import az.xezertv.employeems.entity.Employee;
import az.xezertv.employeems.model.view.ContractView;
import az.xezertv.employeems.repository.ContractRepository;
import az.xezertv.employeems.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ContractService {

    private final ContractRepository contractRepository;
    private final ModelMapper modelMapper;
    private final EmployeeRepository employeeRepository;

    public List<ContractView> findAll() {
        return contractRepository.findAll()
                .stream()
                .map(contract -> modelMapper.map(contract, ContractView.class))
                .collect(Collectors.toList());
    }

    public ContractView findById(Long id) {
        Contract contract = contractRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Contract not found with id - %s", id)
        ));
        return modelMapper.map(contractRepository.save(contract), ContractView.class);
    }


    public ContractDto save(ContractDto contractDto) {
        Employee employee= employeeRepository.findById(contractDto.getEmployeeId()).orElseThrow(()->new NotFoundException(
                String.format("Employee not found with id - %s", contractDto.getEmployeeId())
        ));


        Contract contract = modelMapper.map(contractDto, Contract.class);
        contract.setEmployee(employee);

        return modelMapper.map(contractRepository.save(contract), ContractDto.class);
    }

    public void delete(Long id) {
        Contract contract = contractRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Contract not found with id - %s", id)
        ));
        contractRepository.delete(contract);
    }

    public ContractDto update(Long id,ContractDto contractDto) {
        Contract contract = contractRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Contract not found with id - %s", id)
        ));

        modelMapper.map(contractDto, contract, "map");
        contract.setId(id);

        return modelMapper.map(contractRepository.save(contract), ContractDto.class);
    }



}
