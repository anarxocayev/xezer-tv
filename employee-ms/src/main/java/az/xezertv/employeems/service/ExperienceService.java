package az.xezertv.employeems.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.model.dto.ExperienceDto;
import az.xezertv.employeems.entity.Employee;
import az.xezertv.employeems.entity.Experience;
import az.xezertv.employeems.model.view.ExperienceView;
import az.xezertv.employeems.repository.EmployeeRepository;
import az.xezertv.employeems.repository.ExperienceRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ExperienceService {
    private final ExperienceRepository experienceRepository;
    private final EmployeeRepository employeeRepository;
    private final ModelMapper modelMapper;

    public List<ExperienceView> findAll() {
        return experienceRepository.findAll()
                .stream()
                .map(experience -> modelMapper.map(experience, ExperienceView.class))
                .collect(Collectors.toList());
    }

    public ExperienceView findById(Long id) {
        Experience experience = experienceRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(
                String.format("Experience not found with id - %s", id)
        ));

        return modelMapper.map(experienceRepository.save(experience), ExperienceView.class);
    }

    public ExperienceDto save(ExperienceDto experienceDto) {
        Employee employee = employeeRepository
                .findById(experienceDto.getEmployeeId())
                .orElseThrow(() -> new NotFoundException(
                String.format("Employee not found with id - %s", experienceDto.getEmployeeId())
        ));

        Experience experience = modelMapper.map(experienceDto, Experience.class);
        experience.setEmployee(employee);

        LocalDate previousExperienceDate = experience.getPreviousExperienceDate();
        LocalDate startingDate = experience.getStartingDate();

        experience.setTotalExperience(getTotalExperience(previousExperienceDate, startingDate));
        experienceRepository.save(experience);

        return modelMapper.map(experience, ExperienceDto.class);
    }

    public ExperienceDto update(Long id, ExperienceDto experienceDto) {
        Experience experience = experienceRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(
                String.format("Experience not found with id - %s", id)
        ));

        modelMapper.map(experienceDto, experience, "map");
        experience.setId(id);

        return modelMapper.map(experienceRepository.save(experience), ExperienceDto.class);
    }

    public void delete(Long id) {
        Experience experience = experienceRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Experience not found with id - %s", id)
        ));

        experienceRepository.delete(experience);
    }

    public String getTotalExperience(LocalDate previousExperienceDate, LocalDate startingDate) {
        Period period = Period.between(previousExperienceDate, startingDate);
        int year = period.getYears();
        int month = period.getMonths();
        int day = period.getDays();

        String interval;
        interval = String.format("%dil %day %dgun", year, month, day);

        return interval;
    }
}
