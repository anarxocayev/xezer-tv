package az.xezertv.employeems.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.model.dto.OrderDto;
import az.xezertv.employeems.entity.Employee;
import az.xezertv.employeems.entity.Order;
import az.xezertv.employeems.model.view.OrderView;
import az.xezertv.employeems.repository.EmployeeRepository;
import az.xezertv.employeems.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final ModelMapper modelMapper;
    private final EmployeeRepository employeeRepository;

    public List<OrderView> findAll() {
        return orderRepository.findAll().
                stream().
                map(order -> modelMapper.map(order, OrderView.class)).
                collect(Collectors.toList());
    }

    public OrderView findById(Long id) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new NotFoundException
                (String.format("Order not found with id - %s", id)
                ));
        return modelMapper.map(orderRepository.save(order), OrderView.class);
    }

    public OrderDto save(OrderDto orderDto) {
        Employee employee = employeeRepository.findById(orderDto.getEmployeeId()).orElseThrow(() -> new NotFoundException(
                String.format("Employee not found with id - %s", orderDto.getEmployeeId())
        ));

        Order order = modelMapper.map(orderDto, Order.class);
        order.setEmployee(employee);

        return modelMapper.map(orderRepository.save(order), OrderDto.class);
    }

    public OrderDto update(Long id, OrderDto orderDto) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Order not found with id - %s", id)
        ));

        modelMapper.map(orderDto, order, "map");
        order.setId(id);

        return modelMapper.map(orderRepository.save(order), OrderDto.class);
    }

    public void delete(Long id) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Order not found with id - %s", id)
        ));
        orderRepository.delete(order);
    }
}
