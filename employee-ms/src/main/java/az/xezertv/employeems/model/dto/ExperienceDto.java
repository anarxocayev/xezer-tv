package az.xezertv.employeems.model.dto;

import lombok.Data;
import java.time.LocalDate;

@Data
public class ExperienceDto {
    private Long id;
    private LocalDate previousExperienceDate;
    private LocalDate startingDate;
    private String totalExperience;
    private Integer salaryGross;
    private Integer salaryNet;
    private Long employeeId;
}
