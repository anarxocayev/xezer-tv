package az.xezertv.employeems.model.enums;

public enum FamilyStatus {
    MARRIED,
    SINGLE,
    DIVORCED,
    WIDOW
}
