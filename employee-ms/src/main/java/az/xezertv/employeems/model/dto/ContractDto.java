package az.xezertv.employeems.model.dto;

import az.xezertv.employeems.model.enums.ContractKind;
import az.xezertv.employeems.model.enums.ContractTerm;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
public class ContractDto {
    private Long id;

    private LocalDateTime conclusionDate;
    private LocalDateTime expirationDate;

    @Enumerated(EnumType.STRING)
    private ContractKind contractKind;

    @Enumerated(EnumType.STRING)
    private ContractTerm contractTerm;

    private Long ContractNumber;

    private Long laborBookletNumber;

    private Long employeeId;
}
