package az.xezertv.employeems.model.enums;

public enum Gender {
    FEMALE,
    MALE
}
