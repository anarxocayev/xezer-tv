package az.xezertv.employeems.model.enums;

public enum JobClassification {
    WORKER,
    SERVANT
}
