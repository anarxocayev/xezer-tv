package az.xezertv.employeems.model.view;

import az.xezertv.employeems.model.enums.Disability;
import az.xezertv.employeems.model.enums.Education;
import az.xezertv.employeems.model.enums.FamilyStatus;
import az.xezertv.employeems.model.enums.Gender;
import az.xezertv.employeems.model.enums.JobClassification;
import az.xezertv.employeems.model.enums.RelationshipStatus;
import az.xezertv.employeems.model.enums.State;
import az.xezertv.employeems.model.enums.WorkPlace;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class EmployeeView {
    private Long id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private Long age;
    private String identityCardCode;
    private String citizenship;
    private String address;
    private String email;
    private String mobilePhone;
    private Long homePhone;
    private String relativePhone;
    private LocalDate birthdate;
    private String company;
    private String fin;
    private String nationality;
    private String workHours;
    private Long socialInsuranceNumber;
    private Long corporatePhone;
    private Long weeklyWorkHours;
    private Long internalNumber;
    private RelationshipStatus relationshipStatus;
    private Disability disability;
    private Education education;
    private FamilyStatus familyStatus;
    private Gender gender;
    private JobClassification jobClassification;
    private State state;
    private WorkPlace workPlace;
    private List<OrderView> orders;
    private List<ContractView> contracts;
    private List<ExperienceView> experiences;
}
