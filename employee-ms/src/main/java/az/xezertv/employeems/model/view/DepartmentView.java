package az.xezertv.employeems.model.view;

import lombok.Data;

import java.util.List;

@Data
public class DepartmentView {
    private Long id;
    private String name;
    private List<EmployeeView> employees;
}
