package az.xezertv.employeems.model.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class OrderDto {
    private Long id;
    private String orderType;
    private Integer orderNumber;
    private LocalDate orderDate;
    private String responsiblePerson;
    private Long employeeId;
}
