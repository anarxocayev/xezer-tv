package az.xezertv.employeems.model.enums;

public enum RelationshipStatus {
    WIFE,
    HUSBAND,
    BROTHER,
    SISTER
}
