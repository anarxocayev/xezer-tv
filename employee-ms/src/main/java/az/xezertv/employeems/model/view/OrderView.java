package az.xezertv.employeems.model.view;

import lombok.Data;

import java.time.LocalDate;

@Data
public class OrderView {
    private Long id;
    private String orderType;
    private Integer orderNumber;
    private LocalDate orderDate;
    private String responsiblePerson;
}
