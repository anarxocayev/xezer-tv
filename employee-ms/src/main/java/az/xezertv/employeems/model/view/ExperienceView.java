package az.xezertv.employeems.model.view;

import lombok.Data;
import java.time.LocalDate;

@Data
public class ExperienceView {
    private Long id;
    private LocalDate previousExperienceDate;
    private LocalDate startingDate;
    private String totalExperience;
    private Integer salaryGross;
    private Integer salaryNet;
}
