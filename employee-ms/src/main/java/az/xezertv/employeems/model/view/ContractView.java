package az.xezertv.employeems.model.view;

import az.xezertv.employeems.model.enums.ContractKind;
import az.xezertv.employeems.model.enums.ContractTerm;
import lombok.Data;
import java.time.LocalDateTime;

@Data
public class ContractView {

    private Long id;
    private LocalDateTime conclusionDate;
    private LocalDateTime expirationDate;
    private ContractKind contractKind;
    private ContractTerm contractTerm;
    private Long ContractNumber;
    private Long laborBookletNumber;

}
