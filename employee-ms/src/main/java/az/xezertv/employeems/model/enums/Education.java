package az.xezertv.employeems.model.enums;

public enum Education {
    HIGH,
    SECONDARY,
    UNEDUCATED
}
