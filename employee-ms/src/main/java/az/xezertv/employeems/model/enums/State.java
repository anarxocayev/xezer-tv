package az.xezertv.employeems.model.enums;

public enum State {
    COMPLETED,
    INCOMPLETED
}
