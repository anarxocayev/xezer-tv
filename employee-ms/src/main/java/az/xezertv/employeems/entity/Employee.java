package az.xezertv.employeems.entity;

import az.xezertv.employeems.model.enums.Disability;
import az.xezertv.employeems.model.enums.Education;
import az.xezertv.employeems.model.enums.FamilyStatus;
import az.xezertv.employeems.model.enums.Gender;
import az.xezertv.employeems.model.enums.JobClassification;
import az.xezertv.employeems.model.enums.RelationshipStatus;
import az.xezertv.employeems.model.enums.State;
import az.xezertv.employeems.model.enums.WorkPlace;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "employees")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "age")
    private Long age;

    @Column(name = "identity_card_code")
    private String identityCardCode;

    @Column(name = "citizenship")
    private String citizenship;

    @Column(name = "address")
    private String address;

    @Column(name = "email")
    private String email;

    @Column(name = "mobile_phone")
    private String mobilePhone;

    @Column(name = "home_phone")
    private Long homePhone;

    @Column(name = "relative_phone")
    private String relativePhone;

    @Column(name = "birthdate")
    private LocalDate birthdate;

    @Column(name = "company")
    private String company;

    @Column(name = "fin")
    private String fin;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "work_hours")
    private String workHours;

    @Column(name = "social_insurance_number")
    private Long socialInsuranceNumber;

    @Column(name = "corporate_phone")
    private Long corporatePhone;

    @Column(name = "weekly_work_hours")
    private Long weeklyWorkHours;

    @Column(name = "internal_number")
    private Long internalNumber;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    //enums

    @Column(name = "relationship_status")
    @Enumerated(EnumType.STRING)
    private RelationshipStatus relationshipStatus;

    @Column(name = "disability")
    @Enumerated(EnumType.STRING)
    private Disability disability;

    @Column(name = "education")
    @Enumerated(EnumType.STRING)
    private Education education;

    @Column(name = "family_status")
    @Enumerated(EnumType.STRING)
    private FamilyStatus familyStatus;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "job_classification")
    @Enumerated(EnumType.STRING)
    private JobClassification jobClassification;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private State state;

    @Column(name = "work_place")
    @Enumerated(EnumType.STRING)
    private WorkPlace workPlace;

    //relations
    @ManyToOne(fetch = FetchType.LAZY)
    @ToString.Exclude
    @JsonIgnore
    private Position position;

    @ManyToOne(fetch = FetchType.LAZY)
    @ToString.Exclude
    @JsonIgnore
    private Department department;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    @Fetch(FetchMode.JOIN)
    private List<Order> orders = new ArrayList<>();

    @OneToOne(mappedBy = "employee")
    private Experience experience;


    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_contract_id")
    @Fetch(FetchMode.JOIN)
    private List<Contract> contractList = new ArrayList<>();

}
