package az.xezertv.employeems.entity;

import az.xezertv.employeems.model.enums.ContractKind;
import az.xezertv.employeems.model.enums.ContractTerm;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "contracts")
public class Contract {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "conclusion_date")
    private LocalDateTime conclusionDate;

    @Column(name = "expiration_date")
    private LocalDateTime expirationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "contract_kind")
    private ContractKind contractKind;

    @Enumerated(EnumType.STRING)
    @Column(name = "contract_term")
    private ContractTerm contractTerm;

    @Column(name = "contract_number")
    private Long ContractNumber;

    @Column(name = "labor_booklet_number")
    private Long laborBookletNumber;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;


    @ManyToOne(fetch = FetchType.LAZY)
    @ToString.Exclude
    private Employee employee;

}
