package az.xezertv.employeems.controller;

import az.xezertv.employeems.model.dto.ContractDto;
import az.xezertv.employeems.model.view.ContractView;
import az.xezertv.employeems.service.ContractService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/contract")
@RequiredArgsConstructor
public class ContractController {

    private final ContractService contractService;

    @GetMapping
    public ResponseEntity<List<ContractView>> findAll() {
        return ResponseEntity.ok(contractService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ContractView> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(contractService.findById(id));
    }

    @PostMapping
    public ResponseEntity<ContractDto> save(@RequestBody ContractDto contractDto) {
        return ResponseEntity.ok(contractService.save(contractDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ContractDto> update(@PathVariable("id") Long id, @RequestBody ContractDto contractDto) {
        return ResponseEntity.ok(contractService.update(id,contractDto));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        contractService.delete(id);
    }

}
