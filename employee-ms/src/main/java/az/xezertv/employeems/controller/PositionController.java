package az.xezertv.employeems.controller;

import az.xezertv.employeems.model.dto.PositionDto;
import az.xezertv.employeems.service.PositionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/positions")
public class PositionController {

    private final PositionService positionService;

    @GetMapping
    public ResponseEntity<List<PositionDto>> findAll() {
        return ResponseEntity.ok(positionService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PositionDto> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(positionService.getById(id));
    }

    @PostMapping
    public ResponseEntity<PositionDto> save(@RequestBody PositionDto positionDto) {
        return ResponseEntity.ok(positionService.createPosition(positionDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PositionDto> update(@PathVariable("id") Long id, @RequestBody PositionDto positionDto) {
        return ResponseEntity.ok(positionService.updatePosition(id, positionDto));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        positionService.deletePosition(id);
    }
}
