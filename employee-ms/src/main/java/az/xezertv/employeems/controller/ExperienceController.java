package az.xezertv.employeems.controller;

import az.xezertv.employeems.model.dto.ExperienceDto;
import az.xezertv.employeems.model.view.ExperienceView;
import az.xezertv.employeems.service.ExperienceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/experiences")
@RequiredArgsConstructor
public class ExperienceController {
    private final ExperienceService experienceService;

    @GetMapping
    public ResponseEntity<List<ExperienceView>> findAll() {
        return ResponseEntity.ok(experienceService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ExperienceView> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(experienceService.findById(id));
    }

    @PostMapping
    public ResponseEntity<ExperienceDto> save(@RequestBody ExperienceDto experienceDto) {
        return ResponseEntity.ok(experienceService.save(experienceDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ExperienceDto> update(@PathVariable("id") Long id, @RequestBody ExperienceDto experienceDto) {
        return ResponseEntity.ok(experienceService.update(id, experienceDto));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        experienceService.delete(id);
    }
}
