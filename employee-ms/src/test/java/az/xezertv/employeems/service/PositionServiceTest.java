package az.xezertv.employeems.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.model.dto.PositionDto;
import az.xezertv.employeems.entity.Position;
import az.xezertv.employeems.repository.PositionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PositionServiceTest {
    @Mock
    PositionRepository positionRepository;

    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    PositionService positionService;

    @Test
    void testMockObjectsAreNotNull() {
        assertNotNull(positionRepository);
        assertNotNull(modelMapper);
        assertNotNull(positionService);
    }

    @Test
    void testFindAllSuccess() {
        when(positionRepository.findAll()).thenReturn(getList());

        List<PositionDto> positionDtos = positionService.getAll();

        assertEquals(positionDtos.size(), 2);
    }

    @Test
    void findByIdSuccess() {
        Position position = new Position();
        position.setId(1L);

        PositionDto positionDto = new PositionDto();
        positionDto.setId(1L);


        when(positionRepository.findById(position.getId())).thenReturn(Optional.of(position));
        when(positionService.getById(positionDto.getId())).thenReturn(positionDto);

        PositionDto positionDto1 = positionService.getById(position.getId());

        assertEquals(position.getId(), positionDto1.getId());

    }

    @Test
    void testFindByIdNotFoundException() {
        Position position = new Position();
        position.setId(1L);

        given(positionRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));

        assertThrows(NotFoundException.class, () -> positionService.getById(position.getId()));
    }

    @Test
    void testDeleteSuccess() {
        Position position = new Position();
        position.setId(1L);

        when(positionRepository.findById(position.getId())).thenReturn(Optional.of(position));

        positionService.deletePosition(position.getId());

        verify(positionRepository).delete(position);
    }

    @Test
    void testDeleteNotFound() {
        Position position = new Position();
        position.setId(1L);


        given(positionRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));

        assertThrows(NotFoundException.class, () -> positionService.deletePosition(position.getId()));
    }

    @Test
    void testCreateEmployeeSuccess() {
        Position position = new Position();
        position.setId(1L);

        PositionDto positionDto = new PositionDto();
        positionDto.setId(1L);

        when(modelMapper.map(positionDto, Position.class)).thenReturn(position);
        when(positionRepository.save(position)).thenReturn(position);
        when(modelMapper.map(position, PositionDto.class)).thenReturn(positionDto);

        PositionDto positionDto1 = positionService.createPosition(positionDto);


        assertEquals(positionDto1.getId(), positionDto.getId());
    }

    @Test
    void testUpdateEmployee() {
        Position position = new Position();
        position.setId(1L);

        PositionDto positionDto = new PositionDto();
        positionDto.setId(1L);

        when(positionRepository.findById(position.getId())).thenReturn(Optional.of(position));
        when(positionService.getById(positionDto.getId())).thenReturn(positionDto);

        PositionDto positionDto1 = positionService.updatePosition(1L, positionDto);

        assertEquals(positionDto1.getId(), positionDto.getId());
    }


    List<Position> getList() {
        List<Position> positions = new ArrayList<>();
        Position position = new Position();
        Position position1 = new Position();

        positions.add(position);
        positions.add(position1);

        return positions;
    }

}


