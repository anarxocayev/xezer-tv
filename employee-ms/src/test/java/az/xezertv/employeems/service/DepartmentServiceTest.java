package az.xezertv.employeems.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.model.dto.DepartmentDto;
import az.xezertv.employeems.entity.Department;
import az.xezertv.employeems.repository.DepartmentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DepartmentServiceTest {
    @Mock
    DepartmentRepository departmentRepository;

    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    DepartmentService departmentService;

    List<Department> findAll() {
        List<Department> departments = new ArrayList<>();
        Department department1 = new Department();
        Department department2 = new Department();

        departments.add(department1);
        departments.add(department2);

        return departments;
    }

    @Test
    void testMockObjectsAreNotNull() {
        assertNotNull(departmentRepository);
        assertNotNull(modelMapper);
        assertNotNull(departmentService);
    }

    @Test
    void testFindAllSuccess() {
        when(departmentRepository.findAll()).thenReturn(findAll());

        List<DepartmentDto> departmentDtos = departmentService.findAll();
        assertEquals(departmentDtos.size(), 2);
    }

    @Test
    void findByIdSuccess() {
        Department department = new Department();
        department.setName("Test");
        department.setId(1L);

        DepartmentDto departmentDto = new DepartmentDto();
        departmentDto.setName("Test");
        departmentDto.setId(1L);

        when(departmentRepository.findById(department.getId())).thenReturn(Optional.of(department));
        when(departmentService.findById(departmentDto.getId())).thenReturn(departmentDto);

        DepartmentDto departmentDto1 = departmentService.findById(department.getId());
        assertEquals(department.getId(), departmentDto1.getId());
    }

    @Test
    void testFindByIdNotFoundException() {
        Department department = new Department();
        department.setId(1L);
        department.setName("Test");

        given(departmentRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        assertThrows(NotFoundException.class, () -> departmentService.findById(department.getId()));
    }

    @Test
    void testSaveSuccess() {
        Department department = new Department();
        department.setName("Test");
        department.setId(1L);

        DepartmentDto departmentDto = new DepartmentDto();
        departmentDto.setName("Test");
        departmentDto.setId(1L);

        when(modelMapper.map(departmentDto, Department.class)).thenReturn(department);
        when(departmentRepository.save(department)).thenReturn(department);
        when(modelMapper.map(department, DepartmentDto.class)).thenReturn(departmentDto);

        DepartmentDto departmentDto1 = departmentService.save(departmentDto);
        assertEquals(departmentDto1.getName(), departmentDto.getName());
    }

    @Test
    void testUpdateSuccess() {
        Department department = new Department();
        department.setName("Test");
        department.setId(1L);

        DepartmentDto departmentDto = new DepartmentDto();
        departmentDto.setName("Test");
        departmentDto.setId(1L);

        when(departmentRepository.findById(department.getId())).thenReturn(Optional.of(department));
        when(departmentService.findById(departmentDto.getId())).thenReturn(departmentDto);

        DepartmentDto departmentDto1 = departmentService.update(1L, departmentDto);
        assertEquals(departmentDto1.getName(), departmentDto.getName());
    }

    @Test
    void testDeleteSuccess() {
        Department department = new Department();
        department.setId(1L);
        department.setName("Test");

        when(departmentRepository.findById(department.getId())).thenReturn(Optional.of(department));
        departmentService.delete(department.getId());
        verify(departmentRepository).delete(department);
    }

    @Test
    void testDeleteNotFound() {
        Department department = new Department();
        department.setId(1L);
        department.setName("Test");

        given(departmentRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        assertThrows(NotFoundException.class, () -> departmentService.delete(department.getId()));
    }
}
