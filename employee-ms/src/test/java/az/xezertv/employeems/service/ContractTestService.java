package az.xezertv.employeems.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.model.dto.ContractDto;
import az.xezertv.employeems.model.dto.EmployeeDto;
import az.xezertv.employeems.entity.Contract;
import az.xezertv.employeems.entity.Employee;
import az.xezertv.employeems.model.view.ContractView;
import az.xezertv.employeems.repository.ContractRepository;
import az.xezertv.employeems.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ContractTestService {

    @Mock
    ContractRepository contractRepository;

    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    ContractService contractService;

    @Mock
    EmployeeRepository employeeRepository;

    @Test
    void testMockObjectsAreNotNull() {
        assertNotNull(modelMapper);
        assertNotNull(contractRepository);
        assertNotNull(contractService);
    }
    List<Contract> getList() {
        List<Contract> contractList = new ArrayList<>();
        Contract contract = new Contract();
        Contract contract1 = new Contract();

        contractList.add(contract);
        contractList.add(contract1);

        return contractList;
    }

    @Test
    void testFindAllSuccess() {
        when(contractRepository.findAll()).thenReturn(getList());

        List<ContractView> employeeList = contractService.findAll();

        assertEquals(employeeList.size(), 2);
    }

    @Test
    void findByIdSuccess() {
        Contract contract = new Contract();
        contract.setLaborBookletNumber(123L);
        contract.setContractNumber(456L);
        contract.setId(1L);

        ContractView contractView = new ContractView();
        contractView.setLaborBookletNumber(123L);
        contractView.setContractNumber(456L);
        contractView.setId(1L);


        when(contractRepository.findById(contract.getId())).thenReturn(Optional.of(contract));
        when(contractService.findById(contractView.getId())).thenReturn(contractView);

        ContractView contractView1 = contractService.findById(contract.getId());

        assertEquals(contract.getId(), contractView1.getId());

    }

    @Test
    void testFindByIdNotFoundException() {
        Contract contract = new Contract();
        contract.setLaborBookletNumber(123L);
        contract.setContractNumber(456L);
        contract.setId(1L);

        given(contractRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));

        assertThrows(NotFoundException.class, () -> contractService.findById(contract.getId()));
    }
    @Test
    void testCreateContractSuccess() {
        Contract contract = new Contract();
        contract.setId(1L);

        ContractDto contractDto = new ContractDto();
        contractDto.setId(1L);

        Employee employee = new Employee();
        employee.setId(1L);


        when(modelMapper.map(contractDto, Contract.class)).thenReturn(contract);
        when(contractRepository.save(contract)).thenReturn(contract);
        when(employeeRepository.findById(contractDto.getEmployeeId())).thenReturn(Optional.of(employee));
        when(modelMapper.map(contract, ContractDto.class)).thenReturn(contractDto);

        ContractDto contractDto1 = contractService.save(contractDto);


        assertEquals(contractDto1.getContractNumber(), contractDto.getContractNumber());
    }


//    @Test
//    void testUpdateContract() {
//        Contract contract = new Contract();
//        contract.setLaborBookletNumber(123L);
//        contract.setContractNumber(456L);
//        contract.setId(1L);
//
//        ContractDto contractDto = new ContractDto();
//        contractDto.setLaborBookletNumber(123L);
//        contractDto.setContractNumber(456L);
//        contractDto.setId(1L);
//
//        when(contractRepository.findById(contract.getId())).thenReturn(Optional.of(contract));
//        when(contractService.findById(contractDto.getId())).thenReturn(contractDto);
//
//        ContractDto contractDto1 = contractService.update(1L,contractDto);
//
//        assertEquals(contractDto1.getContractNumber(),contractDto.getContractNumber());
//    }
//

    @Test
    void testDeleteSuccess() {
        Contract contract = new Contract();
        contract.setLaborBookletNumber(123L);
        contract.setContractNumber(456L);
        contract.setId(1L);

        when(contractRepository.findById(contract.getId())).thenReturn(Optional.of(contract));

        contractService.delete(contract.getId());

        verify(contractRepository).delete(contract);
    }

    @Test
    void testDeleteNotFound() {
        Contract contract = new Contract();
        contract.setLaborBookletNumber(123L);
        contract.setContractNumber(456L);
        contract.setId(1L);

        given(contractRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));

        assertThrows(NotFoundException.class, () -> contractService.delete(contract.getId()));
    }

}
