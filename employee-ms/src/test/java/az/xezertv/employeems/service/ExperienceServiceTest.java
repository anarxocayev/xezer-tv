package az.xezertv.employeems.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.model.dto.ExperienceDto;
import az.xezertv.employeems.entity.Experience;
import az.xezertv.employeems.repository.ExperienceRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExperienceServiceTest {
    @Mock
    ExperienceRepository experienceRepository;

    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    ExperienceService experienceService;

    List<Experience> findAll() {
        List<Experience> experiences = new ArrayList<>();
        Experience experience1 = new Experience();
        Experience experience2 = new Experience();

        experiences.add(experience1);
        experiences.add(experience2);

        return experiences;
    }

    @Test
    void testMockObjectsAreNotNull() {
        assertNotNull(experienceRepository);
        assertNotNull(modelMapper);
        assertNotNull(experienceService);
    }

    @Test
    void testFindAllSuccess() {
        when(experienceRepository.findAll()).thenReturn(findAll());

        List<ExperienceDto> experienceDtos = experienceService.findAll();
        assertEquals(experienceDtos.size(), 2);
    }

    @Test
    void findByIdSuccess() {
        Experience experience = new Experience();
        experience.setTotalExperience("Test");
        experience.setId(1L);

        ExperienceDto experienceDto = new ExperienceDto();
        experienceDto.setTotalExperience("Test");
        experienceDto.setId(1L);

        when(experienceRepository.findById(experience.getId())).thenReturn(Optional.of(experience));
        when(experienceService.findById(experienceDto.getId())).thenReturn(experienceDto);

        ExperienceDto experienceDto1 = experienceService.findById(experience.getId());
        assertEquals(experience.getId(), experienceDto1.getId());
    }

    @Test
    void testFindByIdNotFoundException() {
        Experience experience = new Experience();
        experience.setTotalExperience("Test");
        experience.setId(1L);

        given(experienceRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        assertThrows(NotFoundException.class, () -> experienceService.findById(experience.getId()));
    }

    @Test
    void testSaveSuccess() {
        Experience experience = new Experience();
        experience.setTotalExperience("Test");
        experience.setId(1L);

        ExperienceDto experienceDto = new ExperienceDto();
        experienceDto.setTotalExperience("Test");
        experienceDto.setId(1L);

        when(modelMapper.map(experienceDto, Experience.class)).thenReturn(experience);
        when(experienceRepository.save(experience)).thenReturn(experience);
        when(modelMapper.map(experience, ExperienceDto.class)).thenReturn(experienceDto);

        ExperienceDto experienceDto1 = experienceService.save(experienceDto);
        assertEquals(experienceDto1.getTotalExperience(), experienceDto.getTotalExperience());
    }

    @Test
    void testUpdateSuccess() {
        Experience experience = new Experience();
        experience.setTotalExperience("Test");
        experience.setId(1L);

        ExperienceDto experienceDto = new ExperienceDto();
        experienceDto.setTotalExperience("Test");
        experienceDto.setId(1L);

        when(experienceRepository.findById(experience.getId())).thenReturn(Optional.of(experience));
        when(experienceService.findById(experienceDto.getId())).thenReturn(experienceDto);

        ExperienceDto experienceDto1 = experienceService.update(1L, experienceDto);
        assertEquals(experienceDto1.getTotalExperience(), experienceDto.getTotalExperience());
    }

    @Test
    void testDeleteSuccess() {
        Experience experience = new Experience();
        experience.setId(1L);
        experience.setTotalExperience("Test");

        when(experienceRepository.findById(experience.getId())).thenReturn(Optional.of(experience));
        experienceService.delete(experience.getId());
        verify(experienceRepository).deleteById(experience.getId());
    }

    @Test
    void testDeleteNotFound() {
        Experience experience = new Experience();
        experience.setId(1L);
        experience.setTotalExperience("Test");

        given(experienceRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        assertThrows(NotFoundException.class, () -> experienceService.delete(experience.getId()));
    }
}
