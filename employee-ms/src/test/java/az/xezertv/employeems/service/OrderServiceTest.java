package az.xezertv.employeems.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.model.dto.OrderDto;
import az.xezertv.employeems.entity.Employee;
import az.xezertv.employeems.entity.Order;
import az.xezertv.employeems.model.view.OrderView;
import az.xezertv.employeems.repository.EmployeeRepository;
import az.xezertv.employeems.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    @Mock
    OrderRepository orderRepository;

    @Mock
    EmployeeRepository employeeRepository;

    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    OrderService orderService;


    @Test
    void testMockObjectsAreNotNull() {
        assertNotNull(modelMapper);
        assertNotNull(orderRepository);
        assertNotNull(orderService);
    }


    @Test
    void testFindAllSuccess() {
        when(orderRepository.findAll()).thenReturn(getList());

        List<OrderView> orderViews = orderService.findAll();

        assertEquals(orderViews.size(), 2);
    }

    @Test
    void findByIdSuccess() {
        Order order = new Order();
        order.setId(1L);

        OrderView orderView = new OrderView();
        orderView.setId(1L);


        when(orderRepository.findById(order.getId())).thenReturn(Optional.of(order));
        when(orderService.findById(orderView.getId())).thenReturn(orderView);

        OrderView orderView1 = orderService.findById(order.getId());

        assertEquals(order.getId(), orderView1.getId());

    }

    @Test
    void testFindByIdNotFoundException() {
        Order order = new Order();
        order.setId(1L);

        given(orderRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));

        assertThrows(NotFoundException.class, () -> orderService.findById(order.getId()));
    }

    @Test
    void testDeleteSuccess() {
        Order order = new Order();
        order.setId(1L);

        when(orderRepository.findById(order.getId())).thenReturn(Optional.of(order));

        orderService.delete(order.getId());

        verify(orderRepository).delete(order);
    }

    @Test
    void testDeleteNotFound() {
        Order order = new Order();
        order.setId(1L);



        given(orderRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));

        assertThrows(NotFoundException.class, () -> orderService.delete(order.getId()));
    }

    @Test
    void testCreateEmployeeSuccess() throws NotFoundException{
        Order order = new Order();
        order.setId(1L);

        OrderDto orderDto = new OrderDto();
        orderDto.setId(1L);

        Employee employee = new Employee();
        employee.setId(1L);

        when(modelMapper.map(orderDto, Order.class)).thenReturn(order);
        when(orderRepository.save(order)).thenReturn(order);
        when(employeeRepository.findById(orderDto.getEmployeeId())).thenReturn(Optional.of(employee));
        when(modelMapper.map(order, OrderDto.class)).thenReturn(orderDto);

        OrderDto orderDto1 = orderService.save(orderDto);


        assertEquals(orderDto1.getId(), orderDto.getId());
    }

//    @Test
//    void testUpdateEmployee() {
//        Order order = new Order();
//        order.setId(1L);
//
//        OrderDto orderDto = new OrderDto();
//        orderDto.setId(1L);
//
//        OrderView orderView = new OrderView();
//        orderView.setId(1L);
//
//        when(orderRepository.findById(order.getId())).thenReturn(Optional.of(order));
//        when(orderService.findById(orderView.getId())).thenReturn(orderView);
//
//        OrderDto orderDto1 = orderService.update(1L,orderDto);
//
//        assertEquals(orderDto1.getId(),orderDto.getId());
//    }


    List<Order> getList() {
        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        Order order1 = new Order();

        orders.add(order);
        orders.add(order1);

        return orders;
    }
}
