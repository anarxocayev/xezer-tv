package az.xezertv.employeems.service;

import az.xezertv.common.exception.NotFoundException;
import az.xezertv.employeems.entity.Employee;
import az.xezertv.employeems.entity.Order;
import az.xezertv.employeems.model.dto.EmployeeDto;
import az.xezertv.employeems.model.view.EmployeeView;
import az.xezertv.employeems.repository.EmployeeRepository;
import az.xezertv.employeems.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {
    @Mock
    EmployeeRepository employeeRepository;

    @Mock
    OrderRepository orderRepository;

    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    EmployeeService employeeService;


    @Test
    void testMockObjectsAreNotNull() {
        assertNotNull(modelMapper);
        assertNotNull(employeeRepository);
        assertNotNull(employeeService);
    }


    @Test
    void testFindAllSuccess() {
        when(employeeRepository.findAll()).thenReturn(getList());

        List<EmployeeView> employeeList = employeeService.getAll();

        assertEquals(employeeList.size(), 2);
    }

    @Test
    void findByIdSuccess() {
        Employee employee = new Employee();
        employee.setId(1L);

        EmployeeView employeeView = new EmployeeView();
        employeeView.setId(1L);

        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        order.setId(1L);
        order.setEmployee(employee);

        orders.add(order);


        when(employeeRepository.findById(employee.getId())).thenReturn(Optional.of(employee));
        when(orderRepository.findAllByEmployee(employee)).thenReturn(orders);
        when(modelMapper.map(employee, EmployeeView.class)).thenReturn(employeeView);

        EmployeeView employeeView1 = employeeService.getById(employee.getId());

        assertEquals(employee.getId(), employeeView1.getId());

    }

    @Test
    void testFindByIdNotFoundException() {
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setAddress("Address");

        given(employeeRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));

        assertThrows(NotFoundException.class, () -> employeeService.getById(employee.getId()));
    }

    @Test
    void testDeleteSuccess() {
        Employee employee = new Employee();
        employee.setAddress("Address");
        employee.setId(1L);

        when(employeeRepository.findById(employee.getId())).thenReturn(Optional.of(employee));

        employeeService.deleteEmployee(employee.getId());

        verify(employeeRepository).delete(employee);
    }

    @Test
    void testDeleteNotFound() {
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setAddress("Address");


        given(employeeRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));

        assertThrows(NotFoundException.class, () -> employeeService.deleteEmployee(employee.getId()));
    }

    @Test
    void testCreateEmployeeSuccess() {
        Employee employee = new Employee();
        employee.setAddress("Address");
        employee.setEmail("Email");
        employee.setId(1L);

        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setAddress("Address");
        employeeDto.setEmail("Email");
        employeeDto.setId(1L);

        when(modelMapper.map(employeeDto, Employee.class)).thenReturn(employee);
        when(employeeRepository.save(employee)).thenReturn(employee);
        when(modelMapper.map(employee, EmployeeDto.class)).thenReturn(employeeDto);

        EmployeeDto employeeDto1 = employeeService.createEmployee(employeeDto);


        assertEquals(employeeDto1.getAddress(), employeeDto.getAddress());
    }

   /* @Test
    void testUpdateEmployee() {
        Employee employee = new Employee();
        employee.setAddress("Address");
        employee.setEmail("Email");
        employee.setId(1L);


        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setAddress("Address");
        employeeDto.setEmail("Email");
        employeeDto.setId(1L);


        when(employeeRepository.findById(employee.getId())).thenReturn(Optional.of(employee));
        when(employeeRepository.save(employee)).thenReturn(employee);
        when(modelMapper.map(employee, EmployeeDto.class)).thenReturn(employeeDto);


        //  when(modelMapper.map(employee,EmployeeView.class)).thenReturn(employeeView);
        //when(orderRepository.findAllByEmployee(employee)).thenReturn(orders);
        //when(modelMapper.map(order, OrderView.class)).thenReturn(orderView);
        //when(modelMapper.map(employee,EmployeeDto.class)).thenReturn(employeeDto);
        //  lenient().when(modelMapper.map(employeeDto, employee, "map")).thenReturn(1);

        // verify(employee,times(1)).modelMapper.map(employeeDto, employee, "map"));
       //verify(modelMapper,times(1)).map(employeeDto, employee, "map");
        //  when(employeeService.getById(employeeView.getId())).thenReturn(employeeView);


        EmployeeDto employeeDto1 = employeeService.updateEmployee(1L, employeeDto);

        assertEquals(employeeDto1.getEmail(), employeeDto.getEmail());
    }*/


    List<Employee> getList() {
        List<Employee> employees = new ArrayList<>();
        Employee employee = new Employee();
        Employee employee1 = new Employee();

        employees.add(employee);
        employees.add(employee1);

        return employees;
    }

}
