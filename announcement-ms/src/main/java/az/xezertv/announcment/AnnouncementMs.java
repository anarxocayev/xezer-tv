package az.xezertv.announcment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnnouncementMs {

    public static void main(String[] args) {
        SpringApplication.run(AnnouncementMs.class, args);
    }
}
