package az.xezertv.announcment.dto;

import az.xezertv.announcment.enums.Operation;
import lombok.Data;

@Data
public class AnnouncementDto {
    private Long id;
    private String title;
    private String description;
    private Operation operation;
    private Long userId;
    private Boolean isActive;
}
