package az.xezertv.announcment.service;

import az.xezertv.announcment.dto.AnnouncementDto;
import az.xezertv.announcment.entity.Announcement;
import az.xezertv.announcment.repository.AnnouncementRepository;
import az.xezertv.common.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AnnouncementService {
    private final AnnouncementRepository announcementRepository;
    private final ModelMapper modelMapper;

    public List<AnnouncementDto> getAll() {
        return announcementRepository.findAll()
                .stream()
                .map(announcement -> modelMapper.map(announcement, AnnouncementDto.class))
                .collect(Collectors.toList());
    }

    public AnnouncementDto getById(Long id) {
        Announcement announcement = announcementRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Announcement not found with id - %s", id)
        ));
        return modelMapper.map(announcementRepository.save(announcement), AnnouncementDto.class);
    }

    public AnnouncementDto createAnnouncement(AnnouncementDto announcementDto) {
        Announcement announcement = modelMapper.map(announcementDto, Announcement.class);
        return modelMapper.map(announcementRepository.save(announcement), AnnouncementDto.class);
    }

    public AnnouncementDto updateAnnouncement(Long id, AnnouncementDto announcementDto) {
        Announcement announcement = announcementRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Announcement not found with id - %s", id)
        ));

        modelMapper.map(announcementDto, announcement, "map");
        announcement.setId(id);

        return modelMapper.map(announcementRepository.save(announcement), AnnouncementDto.class);
    }

    public void deleteAnnouncement(Long id) {
        Announcement announcement = announcementRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("Pass point not found with id - %s", id))
        );

        announcementRepository.deleteById(id);
    }
}
