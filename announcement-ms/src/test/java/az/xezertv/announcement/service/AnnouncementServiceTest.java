package az.xezertv.announcement.service;

import az.xezertv.announcment.dto.AnnouncementDto;
import az.xezertv.announcment.entity.Announcement;
import az.xezertv.announcment.enums.Operation;
import az.xezertv.announcment.repository.AnnouncementRepository;
import az.xezertv.announcment.service.AnnouncementService;
import az.xezertv.common.exception.NotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AnnouncementServiceTest {


    @Mock
    AnnouncementRepository announcementRepository;

    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    AnnouncementService announcementService;


    @Test
    void testMockObjectsAreNotNull() {
        assertNotNull(modelMapper);
        assertNotNull(announcementRepository);
        assertNotNull(announcementService);

    }

    List<Announcement> getList() {
        List<Announcement> announcements = new ArrayList<>();
        Announcement announcement = new Announcement();
        Announcement announcement1 = new Announcement();

        announcements.add(announcement);
        announcements.add(announcement1);

        return announcements;
    }


    @Test
    void testFindAllSuccess() {
        when(announcementRepository.findAll()).thenReturn(getList());

        List<AnnouncementDto> announcementDtos = announcementService.getAll();

        assertEquals(announcementDtos.size(), 2);
    }


    @Test
    void findByIdSuccess() {
        Announcement announcement = new Announcement();
        announcement.setTitle("Title");
        announcement.setDescription("Description");
        announcement.setId(1L);

        AnnouncementDto announcementDto = new AnnouncementDto();
        announcementDto.setTitle("Title");
        announcementDto.setDescription("Description");
        announcementDto.setId(1L);


        when(announcementRepository.findById(announcement.getId())).thenReturn(Optional.of(announcement));
        when(announcementService.getById(announcementDto.getId())).thenReturn(announcementDto);

        AnnouncementDto announcementDto1 = announcementService.getById(announcement.getId());

        assertEquals(announcement.getId(), announcementDto1.getId());

    }

    @Test
    void testFindByIdNotFoundException() {
        Announcement announcement = new Announcement();
        announcement.setId(1L);
        announcement.setTitle("Title");

        given(announcementRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));

        assertThrows(NotFoundException.class, () -> announcementService.getById(announcement.getId()));
    }

    @Test
    void testCreateAnnouncementSuccess() {
        AnnouncementDto announcementDto = new AnnouncementDto();
        announcementDto.setTitle("Title");
        announcementDto.setDescription("Description");
        announcementDto.setOperation(Operation.PUBLISHED);
        announcementDto.setUserId(12L);
        announcementDto.setIsActive(true);


        Announcement announcement = new Announcement();
        announcement.setTitle("Title");
        announcement.setDescription("Description");
        announcement.setOperation(Operation.PUBLISHED);
        announcement.setUserId(12L);
        announcement.setIsActive(true);
        announcement.setId(1L);

        when(modelMapper.map(announcementDto, Announcement.class)).thenReturn(announcement);
        when(announcementRepository.save(announcement)).thenReturn(announcement);
        when(modelMapper.map(announcement, AnnouncementDto.class)).thenReturn(announcementDto);

        AnnouncementDto announcementDto2 = announcementService.createAnnouncement(announcementDto);


        assertEquals(announcementDto2.getTitle(), announcementDto.getTitle());
    }

    @Test
    void testDeleteSuccess() {
        Announcement announcement = new Announcement();
        announcement.setTitle("Test title");
        announcement.setId(1L);

        when(announcementRepository.findById(announcement.getId())).thenReturn(Optional.of(announcement));

        announcementService.deleteAnnouncement(announcement.getId());

        verify(announcementRepository).deleteById(announcement.getId());
    }

    @Test
    void testDeleteNotFound() {
        Announcement announcement = new Announcement();
        announcement.setId(1L);
        announcement.setTitle("New Title");

        given(announcementRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));

        assertThrows(NotFoundException.class, () -> announcementService.deleteAnnouncement(announcement.getId()));
    }

    @Test
    void testUpdateAnnouncment() {
        Announcement announcement = new Announcement();
        announcement.setTitle("tittle");
        announcement.setDescription("description");
        announcement.setId(1L);

        AnnouncementDto announcementDto = new AnnouncementDto();
        announcementDto.setTitle("tittle");
        announcementDto.setDescription("description");
        announcementDto.setId(1L);

        when(announcementRepository.findById(announcement.getId())).thenReturn(Optional.of(announcement));
        when(announcementService.getById(announcementDto.getId())).thenReturn(announcementDto);

        AnnouncementDto announcementDto1 = announcementService.updateAnnouncement(1L,announcementDto);

        assertEquals(announcementDto1.getTitle(),announcementDto.getTitle());
    }
}

